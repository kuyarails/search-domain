Rails.application.routes.draw do
  root 'dashboard#index'
  get  'domain' => "dashboard#domain", as: :domain
  resources :domain_bookmarks
end
