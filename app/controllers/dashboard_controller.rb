class DashboardController < ApplicationController
  respond_to :js, :html

  def index
    @presenter = SearchPresenter.new params[:q]
  end

  def domain
    @presenter = DomainPresenter.new params[:domain]
  end
end
