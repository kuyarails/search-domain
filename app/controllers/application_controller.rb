class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :load_bookmark

  def load_bookmark
    @bookmarks = DomainBookmark.order("created_at DESC").limit(10)
    @bookmark_count = DomainBookmark.count
  end
end
