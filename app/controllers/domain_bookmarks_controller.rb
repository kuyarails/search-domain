class DomainBookmarksController < ApplicationController
  respond_to :js, :html

  def index
    @presenter = DomainBookmarkPresenter.new params[:q], params[:page]
  end

  def create
    domain_bookmark = DomainBookmark.new params_domain_bookmark
    if domain_bookmark.save
      flash[:notice] = "#{domain_bookmark.domain} is bookmarked"
    else
      render :nothing => true
      flash[:notice] = "Something wrong please try later" 
    end
  end

  def show
  end

  def edit
    @domain_bookmark = DomainBookmark.find params[:id]
  end

  def update
    domain = DomainBookmark.find params[:id]
    if domain.update_attributes params_edit_domain_bookmark
      redirect_to :back
      flash[:notice] = "Updated!"
    else
      redirect_to :back
      flash[:notice] = "Failed"
    end
  end

  def destroy
    domain = DomainBookmark.find params[:id]
    domain.destroy
    redirect_to :back
  end

  private

  def params_domain_bookmark
    params.require(:domain_bookmark).permit(:domain, :host, :subdomain, :path, :availability, :register_url)
  end

  def params_edit_domain_bookmark
    params.require(:domain_bookmark).permit(:price, :buy, :sell)
  end
end
