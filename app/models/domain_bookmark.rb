class DomainBookmark < ActiveRecord::Base
  validates :domain, presence: true
  validates :domain, uniqueness: true
end
