module ApplicationHelper

  def register_link domain
    text = domain['availability'] == "available" ? "Buy Now" : "Make an offer"
    link = domain['register_url'] 
    #optional
    #domain['availability'] == "available" ? domain['register_url'] : UNDEVELOP_URL + "?q=#{domain['domain']}"
    link_to text, link, target: "_blank"
  end

  def register_button_link domain
    text = domain['availability'] == "available" ? "Buy Now" : "Make an offer"
    link = domain['availability'] == "available" ? domain['register_url'] : UNDEVELOP_URL + "?q=#{domain['domain']}"

    link_to text, link, target: "_blank", class: "btn btn-primary btn-lg"
  end

  def register_disable_button_link domain
    link_to domain['availability'].titleize , "#", class: "btn btn-info btn-lg disabled"
  end

  def check_info_link domain
    unless domain['availability'] == "available"
      link_to domain['domain'], domain_path + "?domain=#{domain['domain']}", remote: true
    else
      domain['domain']
    end
  end
end
