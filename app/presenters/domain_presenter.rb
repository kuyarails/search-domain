require 'open-uri'
require 'openssl'

class DomainPresenter
  attr_reader :query

  def initialize(query)
    url = API_DOMAIN_INFO_URL + "&q=#{query}"
    unless query.blank?
      domain = Nokogiri::HTML(open(url)) unless query.blank?
      @domain = parse_json domain
      @who_is = info @domain["whois_url"]
    end
  end

  def parse_json domain
    ActiveSupport::JSON.decode domain.xpath('//p').first
  end

  def domain
    @domain
  end

  def who_is
    @who_is
  end

  def info url
    who_is = Nokogiri::HTML(open(url))
    parse_json_info who_is
  end

  def parse_json_info domain
    domain.css('#registrar_whois').blank? ? "N/A" : domain.css('#registrar_whois').to_s
  end

  def bookmark_count
    DomainBookmark.count
  end
end