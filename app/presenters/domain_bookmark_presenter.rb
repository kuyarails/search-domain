class DomainBookmarkPresenter
  attr_reader :query

  def initialize(query, page = 1)
    @query = query
    @page = page
    @domains = DomainBookmark.ransack(query)
    @count = @domains.result.count
  end

  def count
    @count
  end

  def search_domain
    @domains
  end

  def domains
    Kaminari.paginate_array(@domains.result.order('created_at DESC')).page(@page).per(5)
  end
end