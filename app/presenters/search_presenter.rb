require 'open-uri'
require 'openssl'

class SearchPresenter
  attr_reader :query

  def initialize(query)
    query = query.gsub(" ", "-") unless query.blank?
    url = API_DOMAIN_SEARCH_URL + "&q=#{query}"
    unless query.blank?
      domains = Nokogiri::HTML(open(url)) unless query.blank?
      @domains = parse_json domains
    end
  end

  def parse_json domains
    ActiveSupport::JSON.decode domains.xpath('//p').first
  end

  def domains
    @domains
  end

  def bookmark_count
    DomainBookmark.count
  end
end