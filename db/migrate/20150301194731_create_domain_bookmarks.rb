class CreateDomainBookmarks < ActiveRecord::Migration
  def change
    create_table :domain_bookmarks do |t|
      t.string :domain
      t.string :host
      t.string :subdomain
      t.string :path
      t.string :availability
      t.string :register_url

      t.timestamps
    end
  end
end
