class AddPriceToDomainBookmark < ActiveRecord::Migration
  def change
    add_column :domain_bookmarks, :price, :float
    add_column :domain_bookmarks, :buy, :float
    add_column :domain_bookmarks, :sell, :float
  end
end
