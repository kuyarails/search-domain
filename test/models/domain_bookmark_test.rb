require 'test_helper'

class DomainBookmarkTest < ActiveSupport::TestCase
  test "should not save domain without domain name" do
    domain = DomainBookmark.new
    assert_not domain.save, "Saved the domain without a domain name"
  end

  test "should save domain" do
    domain = DomainBookmark.new(:domain => "cocom.com")
    assert domain.save, "Saved the domain with a domain name"
  end

  test "should not save domain with exist domain record" do
    DomainBookmark.create(:domain => "cocom.com")
    domain = DomainBookmark.new(:domain => "cocom.com")
    assert_not domain.save, "Saved with exist domain name"
  end
end
