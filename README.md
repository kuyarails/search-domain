DomainName 
============

TODO 

[x] Test
[x] Develop
[x] Documentation

#### Description

[x] The purpose is to make easy buyer to enquire the domain
[x] Simple search available domain or inactive domain who already taken by someone. It could help buyer to enquire to the domain owner.
[x] Some domain able bookmark by buyer

#### Features
1. Search domain
2. Domain whois
3. Check domain availbility
4. Bookmark domains
5. Edit price of domain
6. Remove domain

#### Possibility Features
1. Add profile of domain, get from domain whois info
2. check adtive inactive 
3. check alexa rank
4. maybe domain score (domain name quality)

## Template
[x] bootstrap

